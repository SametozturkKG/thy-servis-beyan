package com.thy.servisbeyan.utils.listener;

public interface CategorySelectItemListener {
    void onItemCategorySelectListener(int code, String name);
}
