package com.thy.servisbeyan.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.provider.Settings
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.thy.servisbeyan.R
import com.thy.servisbeyan.data.model.response.LoginResponse
import com.thy.servisbeyan.utils.StaticData.Companion.LOGIN_KEY

class Utils {
    companion object {
        @SuppressLint("HardwareIds")
        fun getDeviceId(context: Context): String {
            return Settings.Secure.getString(
                context.contentResolver,
                Settings.Secure.ANDROID_ID
            )
        }

        fun showToast(context: Context?, message: String?) {
            if (context == null) return
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }

        fun getToken(): String {
            return "Bearer " + PrefManager.get<LoginResponse>(LOGIN_KEY)!!.token
        }

        fun setTextColor(tv: TextView, startPosition: Int, endPosition: Int, color: Int) {
            val spannableStr = SpannableString(tv.text)

            val backgroundColorSpan = ForegroundColorSpan(ContextCompat.getColor(tv.context, color))
            spannableStr.setSpan(
                backgroundColorSpan,
                startPosition,
                endPosition,
                Spanned.SPAN_INCLUSIVE_EXCLUSIVE
            )

            val styleSpanItalic = StyleSpan(Typeface.BOLD)
            spannableStr.setSpan(
                styleSpanItalic,
                startPosition,
                endPosition,
                Spanned.SPAN_INCLUSIVE_EXCLUSIVE
            )

            tv.text = spannableStr
        }
    }
}