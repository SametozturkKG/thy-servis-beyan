package com.thy.servisbeyan.utils.listener;

public interface CheckInItemClickListener {
    void onItemCheckInClick(int id);
}
