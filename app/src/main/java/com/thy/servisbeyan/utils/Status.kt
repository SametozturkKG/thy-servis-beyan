package com.thy.servisbeyan.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}