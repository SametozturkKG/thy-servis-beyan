package com.thy.servisbeyan.utils.listener;

public interface ServiceSelectItemListener {
    void onItemServiceSelectListener(int id, String name, String code, String type);
}
