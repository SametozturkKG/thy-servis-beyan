package com.thy.servisbeyan.utils.listener;

public interface SeatSelectItemListener {
    void onItemSeatSelectListener(int id, String name);
}
