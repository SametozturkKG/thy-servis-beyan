package com.thy.servisbeyan.utils;

import com.thy.servisbeyan.data.model.response.allservice.Data;
import com.thy.servisbeyan.data.model.response.allservice.ServiceCampus;
import com.thy.servisbeyan.data.model.response.allservice.VehicleCategory;

import java.util.ArrayList;
import java.util.List;

public class ResponseSingleton {
    private static final ResponseSingleton responseSingleton = new ResponseSingleton();

    public static ResponseSingleton getInstance() {
        return responseSingleton;
    }

    private List<Data> serviceList;
    private com.thy.servisbeyan.data.model.response.servicedetail.Data myService;

    public List<Data> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Data> serviceList) {
        this.serviceList = serviceList;
    }

    public com.thy.servisbeyan.data.model.response.servicedetail.Data getMyService() {
        return myService;
    }

    public void setMyService(com.thy.servisbeyan.data.model.response.servicedetail.Data myService) {
        this.myService = myService;
    }

    public List<ServiceCampus> getCampusList() {
        List<ServiceCampus> list = new ArrayList<>();

        ServiceCampus campus1 = new ServiceCampus();
        campus1.setName("AHL-ATATÜRK HAVALİMANI");
        campus1.setCode(1367705);

        ServiceCampus campus2 = new ServiceCampus();
        campus2.setName("ISL-İSTANBUL HAVALİMANI");
        campus2.setCode(1367709);

        list.add(campus1);
        list.add(campus2);
        return list;
    }

    public List<VehicleCategory> getCategoryList() {
        List<VehicleCategory> list = new ArrayList<>();

        VehicleCategory category1 = new VehicleCategory();
        category1.setName("RİNG");
        category1.setCode(1481663);

        VehicleCategory category2 = new VehicleCategory();
        category2.setName("VARDİYA SERVİS");
        category2.setCode(1481655);

        VehicleCategory category3 = new VehicleCategory();
        category3.setName("NORMAL SERVİS");
        category3.setCode(1481654);

        list.add(category1);
        list.add(category2);
        list.add(category3);
        return list;
    }
}
