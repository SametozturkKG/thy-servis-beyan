package com.thy.servisbeyan.utils.listener;

import com.thy.servisbeyan.data.model.response.allservice.Data;

public interface ScanBeaconClickListener {
    void onItemScanBeaconClickListener(Data data);
}
