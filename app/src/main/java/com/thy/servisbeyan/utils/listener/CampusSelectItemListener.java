package com.thy.servisbeyan.utils.listener;

public interface CampusSelectItemListener {
    void onItemCampusSelectListener(int code, String name);
}
