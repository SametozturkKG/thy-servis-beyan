package com.thy.servisbeyan.utils

import com.thy.servisbeyan.data.model.response.allservice.Data
import com.turkishairlines.loginclient.AccessManager

class StaticData {
    companion object {
        const val LOGIN_KEY = "login_key"
        var selectDate:String=""
        var selectServiceData: Data? = null
        var accessManager: AccessManager? = null
         var vehicleCategoryCode = -1
         var vehicleCategoryName = ""
         var serviceCampusCode = -1
         var serviceCampusName = ""
    }
}