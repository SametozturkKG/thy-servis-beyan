package com.thy.servisbeyan.ui.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import com.thy.servisbeyan.data.api.ApiHelper
import com.thy.servisbeyan.data.api.ApiUtils
import com.thy.servisbeyan.data.model.response.LoginResponse
import com.thy.servisbeyan.data.model.response.servicedetail.ServiceDetailResponse
import com.thy.servisbeyan.databinding.FragmentHomeBinding
import com.thy.servisbeyan.ui.base.ViewModelFactory
import com.thy.servisbeyan.ui.viewmodel.MainViewModel
import com.thy.servisbeyan.utils.PrefManager
import com.thy.servisbeyan.utils.ResponseSingleton
import com.thy.servisbeyan.utils.StaticData.Companion.LOGIN_KEY
import com.thy.servisbeyan.utils.Status
import com.thy.servisbeyan.utils.Utils.Companion.getToken
import com.thy.servisbeyan.utils.Utils.Companion.showToast


class HomeFragment : Fragment() {
    private var fragmentHomeBinding: FragmentHomeBinding? = null
    private val binding get() = fragmentHomeBinding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentHomeBinding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        getServiceDetailCall()
        getAllService()
    }

    private fun bindView(data: ServiceDetailResponse) {
        binding.homeName.text = PrefManager.get<LoginResponse>(LOGIN_KEY)!!.nameSurname
        binding.homeServiceCode.text = data.data.servisCode
        binding.homeServiceName.text = data.data.servisAdi
        binding.homeServiceType.text = data.data.servisTipi
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fragmentHomeBinding = null
    }

    private lateinit var mainViewModel: MainViewModel

    private fun setupViewModel() {
        mainViewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(ApiUtils.apiEndpoint()))
        ).get(MainViewModel::class.java)
    }

    private fun getServiceDetailCall() {
        mainViewModel.getServiceDetail(getToken())
            .observe(requireActivity(), {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            binding.progressBar.visibility = View.GONE
                            ResponseSingleton.getInstance().myService = resource.data?.data
                            bindView(resource.data!!)
                        }
                        Status.ERROR -> {
                            binding.progressBar.visibility = View.GONE
                            showToast(requireContext(), it.message)
                        }
                        Status.LOADING -> {
                            binding.progressBar.visibility = View.VISIBLE
                        }
                    }
                }
            })
    }

    private fun getAllService() {
        mainViewModel.getAllService(getToken())
            .observe(requireActivity(), {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            resource.data.let { allServiceResponse ->
                                ResponseSingleton.getInstance().serviceList =
                                    allServiceResponse?.data
                            }
                        }
                        Status.ERROR -> {
                            showToast(requireContext(), it.message)
                        }
                        Status.LOADING -> {

                        }
                    }
                }
            })
    }
}