package com.thy.servisbeyan.ui.view.fragment

import android.app.Dialog
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.*
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.thy.servisbeyan.R
import com.thy.servisbeyan.databinding.FragmentAddServiceBinding
import com.thy.servisbeyan.ui.view.activity.BeaconScanActivity
import com.thy.servisbeyan.ui.view.activity.ManuelCheckActivity
import com.thy.servisbeyan.utils.StaticData


class AddServiceFragment : Fragment(), View.OnClickListener {
    private var fragmentAddServiceBinding: FragmentAddServiceBinding? = null
    private val binding get() = fragmentAddServiceBinding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentAddServiceBinding = FragmentAddServiceBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindView()
    }

    private fun bindView() {
        binding.llManuel.setOnClickListener(this)
        binding.llScan.setOnClickListener(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fragmentAddServiceBinding = null
    }

    override fun onClick(v: View?) {
        if (v!!.id == R.id.ll_manuel) {
            StaticData.selectDate=""
            startActivity(Intent(requireActivity(), ManuelCheckActivity::class.java))
        } else if (v.id == R.id.ll_scan) {
            //startActivity(Intent(requireActivity(), BeaconScanActivity::class.java))
            bluetoothCheck()
        }
    }

    private fun bluetoothCheck() {
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (mBluetoothAdapter == null) {
            showCheckDialog(getString(R.string.bluetooth_no), 1)
        } else if (!mBluetoothAdapter.isEnabled) {
            showCheckDialog(getString(R.string.bluetooth_close), 2)
        } else {
            startActivity(Intent(requireActivity(), BeaconScanActivity::class.java))
        }
    }

    private fun showCheckDialog(title: String, type: Int) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog_check_in)
        val tvDescription = dialog.findViewById(R.id.tv_desc) as TextView
        val tvDescription2 = dialog.findViewById(R.id.tv_desc_2) as TextView
        val btnYes = dialog.findViewById(R.id.btn_yes) as Button
        val btnNo = dialog.findViewById(R.id.btn_no) as Button
        tvDescription.text = title
        tvDescription2.visibility = View.GONE
        if (type == 1) {
            btnNo.visibility = View.GONE
            btnYes.text = getString(R.string.ok_button)
        } else if (type == 2) {
            btnNo.visibility = View.VISIBLE
            btnNo.text = getString(R.string.no_button)
        }
        btnYes.setOnClickListener {
            dialog.dismiss()
            openBluetoothSettings()
        }
        btnNo.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
        dialog.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    private fun openBluetoothSettings() {
        val intentBluetooth = Intent()
        intentBluetooth.action = Settings.ACTION_BLUETOOTH_SETTINGS
        startActivity(intentBluetooth)
    }
}