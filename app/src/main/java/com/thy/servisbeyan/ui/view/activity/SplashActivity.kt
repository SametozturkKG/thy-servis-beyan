package com.thy.servisbeyan.ui.view.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.thy.servisbeyan.databinding.ActivitySplashBinding


class SplashActivity : AppCompatActivity() {
    lateinit var activitySplashBinding: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activitySplashBinding = ActivitySplashBinding.inflate(layoutInflater)
        val view = activitySplashBinding.root
        setContentView(view)
        timer()
    }

    private fun timer() {

        Handler(Looper.getMainLooper()).postDelayed({
            finish()
            startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
        }, 2000)
    }
}