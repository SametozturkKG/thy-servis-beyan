package com.thy.servisbeyan.ui.view.activity

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.thy.servisbeyan.R
import com.thy.servisbeyan.ServiceBeyanApplication
import com.thy.servisbeyan.data.api.ApiEndpoint
import com.thy.servisbeyan.data.api.ApiHelper
import com.thy.servisbeyan.data.api.ApiUtils
import com.thy.servisbeyan.data.api.RetrofitClient
import com.thy.servisbeyan.data.model.request.LoginRequest
import com.thy.servisbeyan.data.model.response.LoginResponse
import com.thy.servisbeyan.databinding.ActivityLoginBinding
import com.thy.servisbeyan.ui.base.ViewModelFactory
import com.thy.servisbeyan.ui.view.bottomsheet.WorkPlaceSelectBottomSheet
import com.thy.servisbeyan.ui.viewmodel.LoginViewModel
import com.thy.servisbeyan.utils.PrefManager
import com.thy.servisbeyan.utils.StaticData
import com.thy.servisbeyan.utils.StaticData.Companion.LOGIN_KEY
import com.thy.servisbeyan.utils.Status
import com.thy.servisbeyan.utils.Utils.Companion.getDeviceId
import com.thy.servisbeyan.utils.Utils.Companion.showToast
import com.turkishairlines.loginclient.AccessManager
import com.turkishairlines.loginclient.interfaces.AccessManagerCallBack
import com.turkishairlines.loginclient.interfaces.UserLoginStatusListener
import java.util.concurrent.Executor


class LoginActivity : AppCompatActivity(), View.OnClickListener,
    WorkPlaceSelectBottomSheet.SelectClick, AccessManagerCallBack, UserLoginStatusListener {
    lateinit var activityLoginBinding: ActivityLoginBinding
    private lateinit var loginViewModel: LoginViewModel

    var usernameStr = ""
    var passwordStr = ""
    var selectCompanyId = -1
    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo

    private var accessManager: AccessManager?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityLoginBinding = ActivityLoginBinding.inflate(layoutInflater)
        val view = activityLoginBinding.root
        setContentView(view)
        //initAccessManager()
        //setupViewModel()
        bindView()
        permissionCheck()
        biometric()
        //activityLoginBinding.edtUsername.setText("16825629302")
        //activityLoginBinding.edtPassword.setText("12312")
        //activityLoginBinding.edtUsername.setText("KGTTPOLAT")
        //activityLoginBinding.edtPassword.setText("123456Tp")
        //activityLoginBinding.edtUsername.setText("M_kurt16")
        //activityLoginBinding.edtPassword.setText("Telefon2192#")
    }

    private fun initAccessManager() {
        val application = ServiceBeyanApplication()
        accessManager = application.getAccessManager(applicationContext)!!
        accessManager!!.setCurrentActivity(this@LoginActivity)
        accessManager!!.initAccessManager(applicationContext, this)
        StaticData.accessManager=accessManager
    }


    private fun biometric() {
        executor = ContextCompat.getMainExecutor(this)
        biometricPrompt = BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)
                    startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                }
            })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.biometric_title))
            .setSubtitle("")
            .setNegativeButtonText(getString(R.string.biometric_negative_button))
            .build()
        if (PrefManager.get<LoginResponse>(LOGIN_KEY) != null) {
            biometricPrompt.authenticate(promptInfo)
        }
    }

    private fun permissionCheck() {
        Dexter.withContext(this)
            .withPermissions(
                Manifest.permission.BLUETOOTH,
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest?>?,
                    token: PermissionToken?
                ) {
                }
            }).check()
    }

    private fun setupViewModel() {
        var helper = ApiUtils.apiEndpoint()
         if(StaticData.accessManager!=null){
             helper = StaticData.accessManager!!.retrofit.create(ApiEndpoint::class.java)
        }
        loginViewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(helper))
        ).get(LoginViewModel::class.java)
    }

    private fun bindView() {
        activityLoginBinding.btnLogin.setOnClickListener(this)
        activityLoginBinding.tvWorkPlace.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v!!.id == R.id.btn_login) {
            usernameStr = activityLoginBinding.edtUsername.text.toString()
            passwordStr = activityLoginBinding.edtPassword.text.toString()
            if (validateData()) {
                if (selectCompanyId == 1||selectCompanyId==2||selectCompanyId==3) {
                    accessManagerLogin()
                } else {
                    login()
                }
            } else {
                showToast(this@LoginActivity, getString(R.string.login_validate_error))
            }
        } else if (v.id == R.id.tv_work_place) {
            showBottomSheet()
        }
    }

    private fun validateData(): Boolean {
        if (selectCompanyId != -1) {
            if (usernameStr.isNotBlank() && passwordStr.isNotBlank()) {
                    return true
                }
                return false
        }
        return false
    }

    private fun accessManagerLogin() {
        try {
            initAccessManager()
            accessManager?.authenticate(usernameStr, passwordStr)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun login2(savedUserName: String) {
        val loginRequest = LoginRequest()
        loginRequest.apply {
            username = savedUserName
            password = passwordStr
            deviceId = getDeviceId(this@LoginActivity)
            companyId = selectCompanyId
        }
        loginCall(loginRequest)
    }

    private fun login() {
        val loginRequest = LoginRequest()
        loginRequest.apply {
            username = usernameStr
            password = passwordStr
            deviceId = getDeviceId(this@LoginActivity)
            companyId = selectCompanyId
        }
        StaticData.accessManager = null
        loginCall(loginRequest)
    }

    private fun loginCall(loginRequest: LoginRequest) {
        setupViewModel()
        loginViewModel.login(loginRequest).observe(this) {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        activityLoginBinding.btnLogin.isEnabled = true
                        activityLoginBinding.progressBar.visibility = View.GONE
                        if (resource.data?.resultCode == 200) {
                            PrefManager.put(resource.data, LOGIN_KEY)
                            finish()
                            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                        } else {
                            showToast(this@LoginActivity, resource.data?.resultMessage)
                        }
                    }
                    Status.ERROR -> {
                        activityLoginBinding.btnLogin.isEnabled = true
                        activityLoginBinding.progressBar.visibility = View.GONE
                        showToast(this@LoginActivity, it.message)
                    }
                    Status.LOADING -> {
                        activityLoginBinding.btnLogin.isEnabled = false
                        activityLoginBinding.progressBar.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    private var valueStr = ""
    override fun onSelectClick(value: Int?) {
        selectCompanyId = value!!
        when (value) {
            1 -> {
                activityLoginBinding.tvInfoText.text=getString(R.string.login_info)
                valueStr = getString(R.string.work_place_1)
                activityLoginBinding.tvInfoText.visibility = View.VISIBLE
                activityLoginBinding.edtUsername.hint = getString(R.string.login_username)
            }
            2 -> {
                activityLoginBinding.tvInfoText.text=getString(R.string.login_info)
                valueStr = getString(R.string.work_place_2)
                activityLoginBinding.tvInfoText.visibility = View.VISIBLE
                activityLoginBinding.edtUsername.hint = getString(R.string.login_username)
            }
            3 -> {
                activityLoginBinding.tvInfoText.text=getString(R.string.login_info)
                valueStr = getString(R.string.work_place_3)
                activityLoginBinding.tvInfoText.visibility = View.VISIBLE
                activityLoginBinding.edtUsername.hint = getString(R.string.login_username)
            }
            4 -> {
                activityLoginBinding.tvInfoText.text=getString(R.string.login_info_2)
                valueStr = getString(R.string.work_place_4)
                activityLoginBinding.edtUsername.hint = getString(R.string.login_username_2)
            }
            5 -> {
                activityLoginBinding.tvInfoText.text=getString(R.string.login_info_2)
                valueStr = getString(R.string.work_place_other)
                activityLoginBinding.edtUsername.hint = getString(R.string.login_username_2)
            }
        }
        activityLoginBinding.tvWorkPlace.text = valueStr
    }

    private fun showBottomSheet() {
        val workPlaceSelectBottomSheet: WorkPlaceSelectBottomSheet =
            WorkPlaceSelectBottomSheet.newInstance()
        workPlaceSelectBottomSheet.show(supportFragmentManager, WorkPlaceSelectBottomSheet.TAG)
    }

    override fun onAuthSuccess(p0: Boolean) {
        val savedUserName = accessManager?.savedUsername
        if (savedUserName != null) {
            login2(savedUserName)
        }
    }

    override fun onAuthFailure(p0: String?) {
        Log.i("error", "error")
    }

    override fun needsCredential() {
        Log.i("error", "error")
    }

    override fun onUserTokenExpired() {
        Log.i("error", "error")
    }

}