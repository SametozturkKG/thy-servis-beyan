package com.thy.servisbeyan.ui.view.fragment

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.DatePicker
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.thy.servisbeyan.R
import com.thy.servisbeyan.data.api.ApiHelper
import com.thy.servisbeyan.data.api.ApiUtils
import com.thy.servisbeyan.data.model.request.MyCheckInRequest
import com.thy.servisbeyan.data.model.response.mycheckin.MyCheckInResponse
import com.thy.servisbeyan.databinding.FragmentProfileBinding
import com.thy.servisbeyan.ui.base.ViewModelFactory
import com.thy.servisbeyan.ui.view.activity.LoginActivity
import com.thy.servisbeyan.ui.view.activity.ManuelCheckActivity
import com.thy.servisbeyan.ui.view.adapter.MyCheckInListAdapter
import com.thy.servisbeyan.ui.viewmodel.MainViewModel
import com.thy.servisbeyan.utils.PrefManager
import com.thy.servisbeyan.utils.StaticData
import com.thy.servisbeyan.utils.StaticData.Companion.LOGIN_KEY
import com.thy.servisbeyan.utils.Status
import com.thy.servisbeyan.utils.Utils.Companion.getToken
import com.thy.servisbeyan.utils.Utils.Companion.showToast
import java.text.SimpleDateFormat
import java.util.*


class ProfileFragment : Fragment(), View.OnClickListener, DatePickerDialog.OnDateSetListener{
    private var fragmentProfileBinding: FragmentProfileBinding? = null
    private val binding get() = fragmentProfileBinding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentProfileBinding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        bindView()
    }

    private fun bindView() {
        binding.llDate.setOnClickListener(this)
        binding.ivLogout.setOnClickListener(this)
        binding.llAddCheckin.setOnClickListener(this)
        getMyCheckIn(dateFormatServer(System.currentTimeMillis()))
        binding.tvDate.text = dateFormatUi(Date())
    }

    private lateinit var mainViewModel: MainViewModel

    private fun setupViewModel() {
        mainViewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(ApiUtils.apiEndpoint()))
        ).get(MainViewModel::class.java)
    }

    private fun getMyCheckIn(date: String) {
        val myCheckInRequest = MyCheckInRequest()
        myCheckInRequest.date = date
        mainViewModel.getMyCheckIn(getToken(), myCheckInRequest)
            .observe(requireActivity(), {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            binding.progressBar.visibility = View.GONE
                            bindData(resource.data!!)
                        }
                        Status.ERROR -> {
                            binding.progressBar.visibility = View.GONE
                            showToast(requireContext(), it.message)
                        }
                        Status.LOADING -> {
                            binding.progressBar.visibility = View.VISIBLE
                        }
                    }
                }
            })
    }

    private fun deleteCheckIn(id: Int) {
        mainViewModel.deleteCheckIn(getToken(), id.toString())
            .observe(requireActivity(), {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            binding.progressBar.visibility = View.GONE
                            showSuccessDialog(resource.data?.resultMessage, resource.data?.result!!)
                        }
                        Status.ERROR -> {
                            binding.progressBar.visibility = View.GONE
                            showSuccessDialog(resource.data?.resultMessage, false)
                        }
                        Status.LOADING -> {
                            binding.progressBar.visibility = View.VISIBLE
                        }
                    }
                }
            })
    }

    private fun bindData(myCheckInResponse: MyCheckInResponse) {
        binding.rvMyCheckIn.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = MyCheckInListAdapter(myCheckInResponse.data, ::onCheckClickListener)
        }
    }

    var deleteId: Int = -1
    private fun onCheckClickListener(id: Int) {
        deleteId = id
        showCheckDialog()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fragmentProfileBinding = null
    }

    override fun onClick(v: View?) {
        if (v!!.id == R.id.ll_date) {
            showPicker()
        } else if (v.id == R.id.iv_logout) {
            requireActivity().finish()
            startActivity(Intent(requireActivity(), LoginActivity::class.java))
            PrefManager.put(null, LOGIN_KEY)
        }else if(v.id==R.id.ll_add_checkin){
            StaticData.selectDate=dateUiToDateServer(binding.tvDate.text.toString())
            startActivity(Intent(requireActivity(), ManuelCheckActivity::class.java))
        }
    }

    lateinit var cal: Calendar
    private fun showPicker() {
        cal = Calendar.getInstance()
        DatePickerDialog(
            requireContext(), this,
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.DAY_OF_MONTH)
        ).show()
    }

    override fun onResume() {
        super.onResume()
        if(StaticData.selectDate != ""){
            getMyCheckIn(StaticData.selectDate)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        StaticData.selectDate=""
    }

    var selectedYear = 0
    var selectedMonth = 0
    var selectedDay = 0
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        cal = Calendar.getInstance()
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        binding.tvDate.text = dateFormatUi(cal.time)
        val selectMonth = month + 1
        getMyCheckIn("$dayOfMonth.$selectMonth.$year")
        selectedYear = year
        selectedMonth = selectMonth
        selectedDay = dayOfMonth
    }

    @SuppressLint("SimpleDateFormat")
    private fun dateFormatUi(date: Date): String {
        val sdf = SimpleDateFormat("dd MMMM yyyy")
        return sdf.format(date)
    }

    @SuppressLint("SimpleDateFormat")
    private fun dateFormatServer(date: Long): String {
        val sdf = SimpleDateFormat("dd.MM.yyyy")
        return sdf.format(date)
    }

    @SuppressLint("SimpleDateFormat")
    private fun dateUiToDateServer(date: String):String{
        var format = SimpleDateFormat("dd MMMM yyyy")
        val newDate = format.parse(date)

        format = SimpleDateFormat("dd.MM.yyyy")
        return format.format(newDate)
    }

    private fun showSuccessDialog(title: String?, result: Boolean) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog_check_in_success)
        val tvDescription = dialog.findViewById(R.id.tv_desc) as TextView
        val ivIcon = dialog.findViewById(R.id.iv_icon) as ImageView
        if (!result) {
            ivIcon.setImageResource(R.drawable.ic_info)
        } else {
            ivIcon.setImageResource(R.drawable.ic_success)
        }

        val btnOk = dialog.findViewById(R.id.btn_ok) as Button
        tvDescription.text = title
        btnOk.setOnClickListener {
            dialog.dismiss()
            if (result) {
                if (selectedYear == 0) {
                    getMyCheckIn(dateFormatServer(System.currentTimeMillis()))
                } else {
                    getMyCheckIn("$selectedDay.$selectedMonth.$selectedYear")
                }
            }
        }
        dialog.show()
        dialog.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    private fun showCheckDialog() {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog_check_in)
        val tvDescription = dialog.findViewById(R.id.tv_desc) as TextView
        val tvDescription2 = dialog.findViewById(R.id.tv_desc_2) as TextView
        val btnYes = dialog.findViewById(R.id.btn_yes) as Button
        val btnNo = dialog.findViewById(R.id.btn_no) as Button
        tvDescription.visibility = View.GONE
        tvDescription2.text = getString(R.string.delete_check_in_dialog_title)
        btnYes.setOnClickListener {
            dialog.dismiss()
            deleteCheckIn(deleteId)
        }
        btnNo.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
        dialog.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

}