package com.thy.servisbeyan.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.thy.servisbeyan.data.api.ApiHelper
import com.thy.servisbeyan.data.repository.LoginRepository
import com.thy.servisbeyan.data.repository.MainRepository
import com.thy.servisbeyan.ui.viewmodel.LoginViewModel
import com.thy.servisbeyan.ui.viewmodel.MainViewModel

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val apiHelper: ApiHelper) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(LoginRepository(apiHelper)) as T
        }
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(MainRepository(apiHelper)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}
