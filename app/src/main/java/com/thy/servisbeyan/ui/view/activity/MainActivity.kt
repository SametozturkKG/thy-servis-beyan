package com.thy.servisbeyan.ui.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.thy.servisbeyan.R
import com.thy.servisbeyan.data.api.ApiHelper
import com.thy.servisbeyan.data.api.ApiUtils
import com.thy.servisbeyan.databinding.ActivityMainBinding
import com.thy.servisbeyan.ui.base.ViewModelFactory
import com.thy.servisbeyan.ui.viewmodel.LoginViewModel
import com.thy.servisbeyan.ui.viewmodel.MainViewModel
import com.thy.servisbeyan.utils.PrefManager
import com.thy.servisbeyan.utils.StaticData
import com.thy.servisbeyan.utils.Status
import com.thy.servisbeyan.utils.Utils
import com.thy.servisbeyan.utils.Utils.Companion.showToast


class MainActivity : AppCompatActivity() {
    lateinit var activityMainBinding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        val view = activityMainBinding.root
        setContentView(view)
        setUpNavigation()
    }

    private fun setUpNavigation() {
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.navigation_host_fragment) as NavHostFragment?
        NavigationUI.setupWithNavController(
            activityMainBinding.bottomTabMenu,
            navHostFragment!!.navController
        )
    }
}