package com.thy.servisbeyan.ui.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kontakt.sdk.android.common.profile.ISecureProfile
import com.thy.servisbeyan.data.model.response.allservice.Data
import com.thy.servisbeyan.databinding.ItemServiceListBinding
import com.thy.servisbeyan.utils.ResponseSingleton
import com.thy.servisbeyan.utils.listener.ScanBeaconClickListener

class BeaconListAdapter(
    private var currentServiceList: MutableList<Data>,
    private val scanBeaconClickListener: ScanBeaconClickListener
) :
    RecyclerView.Adapter<BeaconListAdapter.ItemViewHolder>() {
    class ItemViewHolder(private val itemServiceListBinding: ItemServiceListBinding) :
        RecyclerView.ViewHolder(itemServiceListBinding.root) {
        fun bindData(value: Data, scanBeaconClickListener: ScanBeaconClickListener) {
            itemServiceListBinding.itemServiceCode.text = value.servisCode
            itemServiceListBinding.itemServiceName.text = value.servisAdi
            itemServiceListBinding.itemServiceType.text = value.servisTipi
            itemServiceListBinding.root.setOnClickListener {
                scanBeaconClickListener.onItemScanBeaconClickListener(value)
            }
        }
    }

    fun updateList(item: ISecureProfile) {
        val serviceList = ResponseSingleton.getInstance().serviceList
        for (service in serviceList) {
            if (service.beaconList.isNotEmpty()) {
                for (beacon in service.beaconList) {
                    if (beacon.equals(item.uniqueId, true)) {
                        service.beacon = item
                        currentServiceList.add(service)
                    }
                }
            }
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemBinding =
            ItemServiceListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindData(currentServiceList[position], scanBeaconClickListener)
    }

    override fun getItemCount(): Int {
        return currentServiceList.size
    }
}

