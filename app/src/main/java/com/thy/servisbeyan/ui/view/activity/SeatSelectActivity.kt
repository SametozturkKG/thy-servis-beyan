package com.thy.servisbeyan.ui.view.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.thy.servisbeyan.R
import com.thy.servisbeyan.data.api.ApiHelper
import com.thy.servisbeyan.data.api.ApiUtils
import com.thy.servisbeyan.data.model.request.CheckInRequest
import com.thy.servisbeyan.databinding.ActivitySeatSelectBinding
import com.thy.servisbeyan.ui.base.ViewModelFactory
import com.thy.servisbeyan.ui.viewmodel.MainViewModel
import com.thy.servisbeyan.utils.ResponseSingleton
import com.thy.servisbeyan.utils.StaticData
import com.thy.servisbeyan.utils.StaticData.Companion.selectServiceData
import com.thy.servisbeyan.utils.Status
import com.thy.servisbeyan.utils.Utils

class SeatSelectActivity : AppCompatActivity(), View.OnClickListener, TextWatcher {
    lateinit var activitySeatSelectBinding: ActivitySeatSelectBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activitySeatSelectBinding = ActivitySeatSelectBinding.inflate(layoutInflater)
        setContentView(activitySeatSelectBinding.root)
        bindView()
        setupViewModel()
        setScreenType()
    }

    @SuppressLint("SetTextI18n")
    private fun bindView() {
        activitySeatSelectBinding.ivBackButton.setOnClickListener(this)
        activitySeatSelectBinding.btnDirection1.setOnClickListener(this)
        activitySeatSelectBinding.btnDirection2.setOnClickListener(this)
        activitySeatSelectBinding.btnCheckIn.setOnClickListener(this)
        activitySeatSelectBinding.btnVardiya1.setOnClickListener(this)
        activitySeatSelectBinding.btnVardiya2.setOnClickListener(this)
        activitySeatSelectBinding.btnVardiya3.setOnClickListener(this)
        activitySeatSelectBinding.edtSeat.addTextChangedListener(this)
        activitySeatSelectBinding.tvSeatToolbarTitle.text =
            "${selectServiceData!!.servisCode} - ${selectServiceData!!.servisAdi}"

        addNecessaryAreaStars()
    }

    private fun setScreenType() {
        if ("RİNG".equals(selectServiceData?.vehicleCategory?.name!!, ignoreCase = true)) {
            activitySeatSelectBinding.linearLayout.visibility = View.GONE
            activitySeatSelectBinding.textView3.visibility = View.GONE
            activitySeatSelectBinding.llVardiya.visibility = View.GONE
            activitySeatSelectBinding.tvVardiya.visibility = View.GONE
            directionId = 3

        } else if ("VARDİYA SERVİS".equals(
                selectServiceData?.vehicleCategory?.name!!,
                ignoreCase = true
            )
        ) {
            activitySeatSelectBinding.textView3.visibility = View.VISIBLE
            activitySeatSelectBinding.linearLayout.visibility = View.VISIBLE
            activitySeatSelectBinding.llVardiya.visibility = View.VISIBLE
            activitySeatSelectBinding.tvVardiya.visibility = View.VISIBLE


        } else if ("NORMAL SERVİS".equals(
                selectServiceData?.vehicleCategory?.name!!,
                ignoreCase = true
            )
        ) {
            activitySeatSelectBinding.linearLayout.visibility = View.VISIBLE
            activitySeatSelectBinding.textView3.visibility = View.VISIBLE
            activitySeatSelectBinding.llVardiya.visibility = View.GONE
            activitySeatSelectBinding.tvVardiya.visibility = View.GONE
        }
    }

    private lateinit var mainViewModel: MainViewModel
    private fun setupViewModel() {
        mainViewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(ApiUtils.apiEndpoint()))
        ).get(MainViewModel::class.java)
    }


    private fun checkInCall(checkInRequest: CheckInRequest) {
        mainViewModel.checkIn(Utils.getToken(), checkInRequest)
            .observe(this@SeatSelectActivity, {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            activitySeatSelectBinding.progressBar.visibility = View.GONE
                            showSuccessDialog("${selectServiceData!!.servisCode} - ${selectServiceData!!.servisAdi} - ($serviceSelectType) servisine checkin işlemi yapılmıştır.")
                        }
                        Status.ERROR -> {
                            activitySeatSelectBinding.progressBar.visibility = View.GONE
                            Utils.showToast(this@SeatSelectActivity, it.message)
                        }
                        Status.LOADING -> {
                            activitySeatSelectBinding.progressBar.visibility = View.VISIBLE
                        }
                    }
                }
            })
    }

    private fun showSuccessDialog(title: String) {
        val dialog = Dialog(this@SeatSelectActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog_check_in_success)
        val tvDescription = dialog.findViewById(R.id.tv_desc) as TextView
        val btnOk = dialog.findViewById(R.id.btn_ok) as Button
        tvDescription.text = title
        btnOk.setOnClickListener {
            dialog.dismiss()
            val returnIntent = Intent()
            setResult(Activity.RESULT_OK, returnIntent)
            finish()

        }
        dialog.show()
        dialog.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    private var vardiyaId = 1
    private var vardiyaType1 = ""
    private var seatId = -1
    private var serviceSelectType = ""
    private fun checkIn() {
        val checkInRequest = CheckInRequest()
        checkInRequest.beaconId = ""
        checkInRequest.direction = directionId
        checkInRequest.isManuel = true
        checkInRequest.seatNumber = activitySeatSelectBinding.edtSeat.text.toString().toInt()
        checkInRequest.serviceId = selectServiceData!!.id
        checkInRequest.shiftType = vardiyaId
        checkInCall(checkInRequest)
    }

    private fun validateCheckIn(): Boolean {
        if (seatId != -1) {
            return true
        }
        return false
    }

    private fun showCheckDialog(title: String) {
        val dialog = Dialog(this@SeatSelectActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog_check_in)
        val tvDescription = dialog.findViewById(R.id.tv_desc) as TextView
        val tvDescription2 = dialog.findViewById(R.id.tv_desc_2) as TextView
        val btnYes = dialog.findViewById(R.id.btn_yes) as Button
        val btnNo = dialog.findViewById(R.id.btn_no) as Button
        tvDescription.text = title
        tvDescription2.text = getString(R.string.dialog_desc)
        btnYes.setOnClickListener {
            dialog.dismiss()
            checkIn()
        }
        btnNo.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
        dialog.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    private var directionId = 1
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.iv_back_button -> {
                val returnIntent = Intent()
                setResult(Activity.RESULT_CANCELED, returnIntent)
                finish()
            }
            R.id.btn_check_in -> {
                if (validateCheckIn()) {
                    if (StaticData.vehicleCategoryName != "RİNG" && serviceSelectType == "" || StaticData.vehicleCategoryName == "VARDİYA SERVİS" && vardiyaType1 == "") {
                        Utils.showToast(
                            this@SeatSelectActivity,
                            getString(R.string.login_validate_error)
                        )
                    } else {
                        val myService = ResponseSingleton.getInstance().myService
                        if (selectServiceData!!.id == myService.id) {
                            checkIn()
                        } else {
                            showCheckDialog(
                                "${myService.servisCode} - ${myService.servisAdi} - (${myService.servisTipi}) servisine kayıtlısınız, " +
                                        "beyanınızı ${selectServiceData!!.servisCode} - ${selectServiceData!!.servisAdi} - ($serviceSelectType) servisine yapıyorsunuz."
                            )
                        }
                    }
                } else {
                    Utils.showToast(
                        this@SeatSelectActivity,
                        getString(R.string.login_validate_error)
                    )
                }
            }
            R.id.btn_direction_1 -> {
                directionId = 1
                serviceSelectType = "Gidiş"
                activitySeatSelectBinding.btnDirection1.background = ContextCompat.getDrawable(
                    this@SeatSelectActivity,
                    R.drawable.bg_select_direction
                )
                activitySeatSelectBinding.btnDirection2.background = ContextCompat.getDrawable(
                    this@SeatSelectActivity,
                    R.drawable.bg_un_select_direction
                )

                activitySeatSelectBinding.btnDirection1.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.white
                    )
                )
                activitySeatSelectBinding.btnDirection2.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.blue
                    )
                )
            }
            R.id.btn_direction_2 -> {
                directionId = 2
                serviceSelectType = "Dönüş"
                activitySeatSelectBinding.btnDirection1.background = ContextCompat.getDrawable(
                    this@SeatSelectActivity,
                    R.drawable.bg_un_select_direction
                )
                activitySeatSelectBinding.btnDirection2.background = ContextCompat.getDrawable(
                    this@SeatSelectActivity,
                    R.drawable.bg_select_direction
                )
                activitySeatSelectBinding.btnDirection1.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.blue
                    )
                )
                activitySeatSelectBinding.btnDirection2.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.white
                    )
                )

            }

            R.id.btn_vardiya_1 -> {
                vardiyaId = 1
                vardiyaType1 = "Sabah"
                activitySeatSelectBinding.btnVardiya1.background = ContextCompat.getDrawable(
                    this@SeatSelectActivity,
                    R.drawable.bg_select_direction
                )
                activitySeatSelectBinding.btnVardiya2.background = ContextCompat.getDrawable(
                    this@SeatSelectActivity,
                    R.drawable.bg_un_select_direction
                )

                activitySeatSelectBinding.btnVardiya3.background = ContextCompat.getDrawable(
                    this@SeatSelectActivity,
                    R.drawable.bg_un_select_direction
                )

                activitySeatSelectBinding.btnVardiya1.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.white
                    )
                )
                activitySeatSelectBinding.btnVardiya2.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.blue
                    )
                )

                activitySeatSelectBinding.btnVardiya3.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.blue
                    )
                )

            }
            R.id.btn_vardiya_2 -> {
                vardiyaId = 2
                vardiyaType1 = "Öğle"
                activitySeatSelectBinding.btnVardiya1.background = ContextCompat.getDrawable(
                    this@SeatSelectActivity,
                    R.drawable.bg_un_select_direction
                )
                activitySeatSelectBinding.btnVardiya2.background = ContextCompat.getDrawable(
                    this@SeatSelectActivity,
                    R.drawable.bg_select_direction
                )

                activitySeatSelectBinding.btnVardiya3.background = ContextCompat.getDrawable(
                    this@SeatSelectActivity,
                    R.drawable.bg_un_select_direction
                )

                activitySeatSelectBinding.btnVardiya1.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.blue
                    )
                )
                activitySeatSelectBinding.btnVardiya2.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.white
                    )
                )

                activitySeatSelectBinding.btnVardiya3.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.blue
                    )
                )
            }
            R.id.btn_vardiya_3 -> {
                vardiyaId = 3
                vardiyaType1 = "Gece"
                activitySeatSelectBinding.btnVardiya1.background = ContextCompat.getDrawable(
                    this@SeatSelectActivity,
                    R.drawable.bg_un_select_direction
                )
                activitySeatSelectBinding.btnVardiya2.background = ContextCompat.getDrawable(
                    this@SeatSelectActivity,
                    R.drawable.bg_un_select_direction
                )

                activitySeatSelectBinding.btnVardiya3.background = ContextCompat.getDrawable(
                    this@SeatSelectActivity,
                    R.drawable.bg_select_direction
                )

                activitySeatSelectBinding.btnVardiya1.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.blue
                    )
                )
                activitySeatSelectBinding.btnVardiya2.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.blue
                    )
                )

                activitySeatSelectBinding.btnVardiya3.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.white
                    )
                )
            }
        }
    }

    private fun addNecessaryAreaStars() {
        //Utils.setTextColor(activitySeatSelectBinding.tvSeatTitle, 7, 8, R.color.red)
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    private var edtValue = ""
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        edtValue = s.toString()
        if (!"".equals(edtValue, true)) {
            if (edtValue.toInt() > 55 || edtValue.toInt() == 0) {
                seatId = -1
                activitySeatSelectBinding.edtSeat.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.red
                    )
                )
            } else {
                seatId = (s.toString()).toInt()
                activitySeatSelectBinding.edtSeat.setTextColor(
                    ContextCompat.getColor(
                        this@SeatSelectActivity,
                        R.color.black
                    )
                )
            }
        }

    }

    override fun afterTextChanged(s: Editable?) {
    }
}