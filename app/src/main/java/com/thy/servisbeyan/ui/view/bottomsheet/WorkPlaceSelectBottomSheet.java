package com.thy.servisbeyan.ui.view.bottomsheet;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.thy.servisbeyan.R;
import com.thy.servisbeyan.databinding.BottomSheetWorkPlaceBinding;

public class WorkPlaceSelectBottomSheet extends BottomSheetDialogFragment implements View.OnClickListener {
    public static final String TAG = "WorkPlaceSelectBottomSheet";
    BottomSheetWorkPlaceBinding bottomSheetWorkPlaceBinding;
    private SelectClick selectClick;

    public static WorkPlaceSelectBottomSheet newInstance() {
        return new WorkPlaceSelectBottomSheet();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        bottomSheetWorkPlaceBinding = BottomSheetWorkPlaceBinding.inflate(getLayoutInflater());
        return bottomSheetWorkPlaceBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bottomSheetWorkPlaceBinding.workPlace1.setOnClickListener(this);
        bottomSheetWorkPlaceBinding.workPlace2.setOnClickListener(this);
        bottomSheetWorkPlaceBinding.workPlace3.setOnClickListener(this);
        bottomSheetWorkPlaceBinding.workPlace4.setOnClickListener(this);
        bottomSheetWorkPlaceBinding.workPlace5.setOnClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bottomSheetWorkPlaceBinding = null;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.work_place_1:
                selectClick.onSelectClick(1);
                break;
            case R.id.work_place_2:
                selectClick.onSelectClick(2);
                break;
            case R.id.work_place_3:
                selectClick.onSelectClick(3);
                break;
            case R.id.work_place_4:
                selectClick.onSelectClick(4);
                break;
            case R.id.work_place_5:
                selectClick.onSelectClick(5);
                break;
        }
        dismiss();
    }

    public interface SelectClick {
        void onSelectClick(Integer value);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SelectClick) {
            selectClick = (SelectClick) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ItemClickListener");
        }
    }
}
