package com.thy.servisbeyan.ui.view.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.kontakt.sdk.android.ble.manager.ProximityManager
import com.kontakt.sdk.android.ble.manager.ProximityManagerFactory
import com.kontakt.sdk.android.ble.manager.listeners.SecureProfileListener
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleSecureProfileListener
import com.kontakt.sdk.android.common.profile.ISecureProfile
import com.thy.servisbeyan.R
import com.thy.servisbeyan.data.api.ApiHelper
import com.thy.servisbeyan.data.api.ApiUtils
import com.thy.servisbeyan.data.model.request.CheckInRequest
import com.thy.servisbeyan.data.model.response.allservice.Data
import com.thy.servisbeyan.databinding.ActivityBeaconScanBinding
import com.thy.servisbeyan.ui.base.ViewModelFactory
import com.thy.servisbeyan.ui.view.adapter.BeaconListAdapter
import com.thy.servisbeyan.ui.viewmodel.MainViewModel
import com.thy.servisbeyan.utils.StaticData.Companion.selectServiceData
import com.thy.servisbeyan.utils.Status
import com.thy.servisbeyan.utils.Utils
import com.thy.servisbeyan.utils.Utils.Companion.showToast
import com.thy.servisbeyan.utils.listener.ScanBeaconClickListener
import java.util.*


class BeaconScanActivity : AppCompatActivity(), View.OnClickListener, ScanBeaconClickListener {
    lateinit var proximityManager: ProximityManager
    lateinit var activityBeaconScanBinding: ActivityBeaconScanBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityBeaconScanBinding = ActivityBeaconScanBinding.inflate(layoutInflater)
        setContentView(activityBeaconScanBinding.root)
        setupViewModel()
        bindView()
        setupAdapter()
        setupBeacon()
        timerScan()
    }

    @SuppressLint("SetTextI18n")
    private fun bindView() {
        activityBeaconScanBinding.tvScanToolbarTitle.text =
            getString(R.string.add_service_auto_1) + " " + getString(R.string.add_service_auto_2)
        activityBeaconScanBinding.btnReScan.setOnClickListener(this)
        activityBeaconScanBinding.ivBackButton.setOnClickListener(this)
    }

    private fun timerScan() {
        Handler(Looper.getMainLooper()).postDelayed({
            stopScanning()
            activityBeaconScanBinding.btnReScan.visibility = View.VISIBLE
            activityBeaconScanBinding.progressBar.visibility = View.GONE
        }, 5000)
        beaconList.clear()
        startScanning()
    }

    private val beaconList = mutableListOf<Data>()
    lateinit var beaconListAdapter: BeaconListAdapter
    private fun setupAdapter() {
        beaconListAdapter = BeaconListAdapter(beaconList, this)

        activityBeaconScanBinding.rvBeacon.apply {
            layoutManager = LinearLayoutManager(this@BeaconScanActivity)
            adapter = beaconListAdapter
        }
    }


    private fun setupBeacon() {
        proximityManager = ProximityManagerFactory.create(this)
        proximityManager.setSecureProfileListener(createIBeaconListener())
    }

    private fun createIBeaconListener(): SecureProfileListener {
        return object : SimpleSecureProfileListener() {
            override fun onProfileDiscovered(profile: ISecureProfile?) {
                super.onProfileDiscovered(profile)
                beaconListAdapter.updateList(profile!!)
            }
        }
    }

    private fun startScanning() {
        proximityManager.connect { proximityManager.startScanning() }
    }

    private fun stopScanning() {
        proximityManager.stopScanning()
        proximityManager.disconnect()
    }

    override fun onDestroy() {
        stopScanning()
        super.onDestroy()
    }

    override fun onClick(v: View?) {
        if (v!!.id == R.id.btn_re_scan) {
            activityBeaconScanBinding.btnReScan.visibility = View.GONE
            activityBeaconScanBinding.progressBar.visibility = View.VISIBLE
            timerScan()
        } else if (v.id == R.id.iv_back_button) {
            finish()
        }
    }

    private lateinit var mainViewModel: MainViewModel
    private fun setupViewModel() {
        mainViewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(ApiUtils.apiEndpoint()))
        ).get(MainViewModel::class.java)
    }

    private var selectService: Data? = null
    override fun onItemScanBeaconClickListener(data: Data) {
        selectService = data
        selectServiceData = selectService
        startActivityForResult(
            Intent(this@BeaconScanActivity, SeatSelectActivity::class.java),
            1111
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1111) {
            if (resultCode == Activity.RESULT_OK) {
                finish()
            }
        }
    }


    private fun checkIn() {
        val checkInRequest = CheckInRequest()
        checkInRequest.beaconId = selectService!!.beacon!!.uniqueId
        checkInRequest.direction = 1
        checkInRequest.isManuel = false
        checkInRequest.seatNumber = 1
        checkInRequest.serviceId = selectService!!.id
        checkInCall(checkInRequest)
    }

    private fun checkInCall(checkInRequest: CheckInRequest) {
        mainViewModel.checkIn(Utils.getToken(), checkInRequest)
            .observe(this@BeaconScanActivity) {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            activityBeaconScanBinding.progressBar.visibility = View.GONE
                            showSuccessDialog("${selectService!!.servisCode} ${selectService!!.servisAdi} (${selectService!!.servisTipi}) servisine checkin işlemi yapılmıştır.")
                        }
                        Status.ERROR -> {
                            activityBeaconScanBinding.progressBar.visibility = View.GONE
                            showToast(this@BeaconScanActivity, it.message)
                        }
                        Status.LOADING -> {
                            activityBeaconScanBinding.progressBar.visibility = View.VISIBLE
                        }
                    }
                }
            }
    }

    private fun showSuccessDialog(title: String) {
        val dialog = Dialog(this@BeaconScanActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog_check_in_success)
        val tvDescription = dialog.findViewById(R.id.tv_desc) as TextView
        val btnOk = dialog.findViewById(R.id.btn_ok) as Button
        tvDescription.text = title
        btnOk.setOnClickListener {
            dialog.dismiss()
            finish()
        }
        dialog.show()
        dialog.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    private fun showCheckDialog(title: String) {
        val dialog = Dialog(this@BeaconScanActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog_check_in)
        val tvDescription = dialog.findViewById(R.id.tv_desc) as TextView
        val tvDescription2 = dialog.findViewById(R.id.tv_desc_2) as TextView
        val btnYes = dialog.findViewById(R.id.btn_yes) as Button
        val btnNo = dialog.findViewById(R.id.btn_no) as Button
        tvDescription.text = title
        tvDescription2.text = getString(R.string.dialog_desc)
        btnYes.setOnClickListener {
            dialog.dismiss()
            checkIn()
        }
        btnNo.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
        dialog.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }
}