package com.thy.servisbeyan.ui.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thy.servisbeyan.databinding.ItemServiceSelectBinding
import com.thy.servisbeyan.utils.listener.SeatSelectItemListener

class SeatSelectAdapter(
    private var seatList: MutableList<Int>,
    private var seatSelectItemListener: SeatSelectItemListener
) :
    RecyclerView.Adapter<SeatSelectAdapter.ItemViewHolder>() {
    class ItemViewHolder(private val itemServiceSelectBinding: ItemServiceSelectBinding) :
        RecyclerView.ViewHolder(itemServiceSelectBinding.root) {
        fun bindData(value: Int, seatSelectItemListener: SeatSelectItemListener) {
            itemServiceSelectBinding.tvServiceName.text = value.toString()
            itemServiceSelectBinding.tvServiceName.setOnClickListener {
                seatSelectItemListener.onItemSeatSelectListener(value, value.toString())
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemBinding =
            ItemServiceSelectBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindData(seatList[position], seatSelectItemListener)
    }

    override fun getItemCount(): Int {
        return seatList.size
    }
}