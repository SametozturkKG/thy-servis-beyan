package com.thy.servisbeyan.ui.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thy.servisbeyan.data.model.response.mycheckin.Data
import com.thy.servisbeyan.databinding.ItemMyCheckInBinding
import com.thy.servisbeyan.utils.listener.CheckInItemClickListener
import kotlin.reflect.KFunction1

class MyCheckInListAdapter(
    private val checkInList: MutableList<Data>,
    private val checkInItemClickListener: KFunction1<@ParameterName(name = "id") Int, Unit>
) :
    RecyclerView.Adapter<MyCheckInListAdapter.ItemViewHolder>() {
    class ItemViewHolder(private val itemMyCheckInBinding: ItemMyCheckInBinding) :
        RecyclerView.ViewHolder(itemMyCheckInBinding.root) {
        fun bindData(data: Data, checkInItemClickListener: CheckInItemClickListener) {
            itemMyCheckInBinding.cvInfo.visibility = View.VISIBLE
            itemMyCheckInBinding.tvServiceCode.text = data.serviceCode
            itemMyCheckInBinding.tvServiceDate.text = data.date
            itemMyCheckInBinding.tvServiceHour.text = data.time
            itemMyCheckInBinding.tvServiceType.text = data.servisType
            itemMyCheckInBinding.tvServiceSeat.text = data.seat
            itemMyCheckInBinding.tvKampusType.text = data.serviceCampus?.name!!.split("-")[0]
            var shiftName = ""
            if (data.shiftType == 1 && data.servisType=="Vardiya"){
                shiftName = "Sabah"
            }else if(data.shiftType == 2 && data.servisType=="Vardiya"){
                shiftName = "Öğle"
            }else if(data.shiftType == 3 && data.servisType=="Vardiya"){
                shiftName = "Gece"
            }
            itemMyCheckInBinding.tvKategoriType.text = shiftName

            var direction = ""
            when (data.direction) {
                "1" -> {
                    direction = "İşe Geliş"
                }
                "2" -> {
                    direction = "Eve Dönüş"
                }
                "3" -> {
                    direction = "Diğer"
                }
            }
            itemMyCheckInBinding.tvServiceDirection.text = direction
            itemView.setOnClickListener {
                checkInItemClickListener.onItemCheckInClick(data.id)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemMyCheckInBinding =
            ItemMyCheckInBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(itemMyCheckInBinding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindData(checkInList[position], checkInItemClickListener)
    }

    override fun getItemCount(): Int {
        return checkInList.size
    }


}