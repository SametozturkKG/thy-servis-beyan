package com.thy.servisbeyan.ui.view.bottomsheet;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.thy.servisbeyan.data.model.response.allservice.Data;
import com.thy.servisbeyan.databinding.BottomSheetServiceSelectBinding;
import com.thy.servisbeyan.ui.view.adapter.ServiceSelectAdapter;
import com.thy.servisbeyan.utils.ResponseSingleton;
import com.thy.servisbeyan.utils.StaticData;
import com.thy.servisbeyan.utils.listener.ServiceSelectItemListener;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ServiceSelectBottomSheet extends BottomSheetDialogFragment implements ServiceSelectItemListener {
    public static final String TAG = "ServiceSelectBottomSheet";
    private ServiceItemClick itemClick;
    BottomSheetServiceSelectBinding bottomSheetServiceSelectBinding;

    public static ServiceSelectBottomSheet newInstance() {
        return new ServiceSelectBottomSheet();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        bottomSheetServiceSelectBinding = BottomSheetServiceSelectBinding.inflate(getLayoutInflater());
        return bottomSheetServiceSelectBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAdapter();
    }

    public void setAdapter() {
        List<Data> serviceList = ResponseSingleton.getInstance().getServiceList();
        List<Data> serviceFilter = new ArrayList<>();
        for (Data item : serviceList){
            if (item.getServiceCampus() != null && item.getVehicleCategory() != null){
                if (item.getServiceCampus().getCode() == StaticData.Companion.getServiceCampusCode() && item.getVehicleCategory().getCode() == StaticData.Companion.getVehicleCategoryCode()){
                    serviceFilter.add(item);
                }
            }

        }
        ServiceSelectAdapter serviceSelectAdapter = new ServiceSelectAdapter(serviceFilter, this);
        bottomSheetServiceSelectBinding.rvService.setLayoutManager(new LinearLayoutManager(requireContext()));
        bottomSheetServiceSelectBinding.rvService.setAdapter(serviceSelectAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bottomSheetServiceSelectBinding = null;
    }

    @Override
    public void onItemServiceSelectListener(int id, String name, String code, String type) {
        itemClick.onServiceItemClick(id, name, code, type);
        dismiss();
    }

    public interface ServiceItemClick {
        void onServiceItemClick(int id, String name, String code, String type);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ServiceItemClick) {
            itemClick = (ServiceItemClick) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ItemClickListener");
        }
    }
}
