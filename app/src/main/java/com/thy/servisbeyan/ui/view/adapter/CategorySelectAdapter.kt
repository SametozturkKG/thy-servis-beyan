package com.thy.servisbeyan.ui.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thy.servisbeyan.data.model.response.allservice.VehicleCategory
import com.thy.servisbeyan.databinding.ItemCategorySelectBinding
import com.thy.servisbeyan.utils.listener.CategorySelectItemListener

class CategorySelectAdapter(
    private var categoryList: List<VehicleCategory>,
    private var categorySelectItemListener: CategorySelectItemListener
) :
    RecyclerView.Adapter<CategorySelectAdapter.ItemViewHolder>() {
    class ItemViewHolder(private val itemCategorySelectBinding: ItemCategorySelectBinding) :
        RecyclerView.ViewHolder(itemCategorySelectBinding.root) {
        @SuppressLint("SetTextI18n")
        fun bindData(
            value: VehicleCategory,
            categorySelectItemListener: CategorySelectItemListener
        ) {
            itemCategorySelectBinding.tvCategoryName.text = value.name
            itemCategorySelectBinding.tvCategoryName.setOnClickListener {
                categorySelectItemListener.onItemCategorySelectListener(
                    value.code,
                    value.name
                )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemBinding =
            ItemCategorySelectBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindData(categoryList[position], categorySelectItemListener)
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }
}