package com.thy.servisbeyan.ui.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thy.servisbeyan.data.model.response.allservice.Data
import com.thy.servisbeyan.data.model.response.allservice.ServiceCampus
import com.thy.servisbeyan.databinding.ItemCampusSelectBinding
import com.thy.servisbeyan.utils.listener.CampusSelectItemListener

class CampusSelectAdapter(
    private var campusList: List<ServiceCampus>,
    private var campusSelectItemListener: CampusSelectItemListener
) :
    RecyclerView.Adapter<CampusSelectAdapter.ItemViewHolder>() {
    class ItemViewHolder(private val itemCampusSelectBinding: ItemCampusSelectBinding) :
        RecyclerView.ViewHolder(itemCampusSelectBinding.root) {
        @SuppressLint("SetTextI18n")
        fun bindData(value: ServiceCampus, campusSelectItemListener: CampusSelectItemListener) {
            itemCampusSelectBinding.tvCampusName.text = value.name
            itemCampusSelectBinding.tvCampusName.setOnClickListener {
                campusSelectItemListener.onItemCampusSelectListener(
                    value.code,
                    value.name
                )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemBinding =
            ItemCampusSelectBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindData(campusList[position], campusSelectItemListener)
    }

    override fun getItemCount(): Int {
        return campusList.size
    }
}