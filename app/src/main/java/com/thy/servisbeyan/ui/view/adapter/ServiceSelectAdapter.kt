package com.thy.servisbeyan.ui.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.thy.servisbeyan.data.model.response.allservice.Data
import com.thy.servisbeyan.databinding.ItemServiceSelectBinding
import com.thy.servisbeyan.utils.listener.ServiceSelectItemListener

class ServiceSelectAdapter(
    private var serviceList: MutableList<Data>,
    private var serviceSelectItemListener: ServiceSelectItemListener
) :
    RecyclerView.Adapter<ServiceSelectAdapter.ItemViewHolder>() {
    class ItemViewHolder(private val itemServiceSelectBinding: ItemServiceSelectBinding) :
        RecyclerView.ViewHolder(itemServiceSelectBinding.root) {
        @SuppressLint("SetTextI18n")
        fun bindData(value: Data, serviceSelectItemListener: ServiceSelectItemListener) {
            itemServiceSelectBinding.tvServiceName.text = "${value.servisCode} - ${value.servisAdi}"
            itemServiceSelectBinding.tvServiceName.setOnClickListener {
                serviceSelectItemListener.onItemServiceSelectListener(
                    value.id,
                    value.servisAdi,
                    value.servisCode,
                    value.servisTipi
                )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemBinding =
            ItemServiceSelectBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ItemViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindData(serviceList[position], serviceSelectItemListener)
    }

    override fun getItemCount(): Int {
        return serviceList.size
    }
}