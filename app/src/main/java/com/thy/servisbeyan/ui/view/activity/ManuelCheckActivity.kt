package com.thy.servisbeyan.ui.view.activity

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import com.thy.servisbeyan.R
import com.thy.servisbeyan.data.api.ApiHelper
import com.thy.servisbeyan.data.api.ApiUtils
import com.thy.servisbeyan.data.model.request.CheckInRequest
import com.thy.servisbeyan.databinding.ActivityManuelCheckBinding
import com.thy.servisbeyan.ui.base.ViewModelFactory
import com.thy.servisbeyan.ui.view.bottomsheet.CampusSelectBottomSheet
import com.thy.servisbeyan.ui.view.bottomsheet.CategorySelectBottomSheet
import com.thy.servisbeyan.ui.view.bottomsheet.ServiceSelectBottomSheet
import com.thy.servisbeyan.ui.viewmodel.MainViewModel
import com.thy.servisbeyan.utils.ResponseSingleton
import com.thy.servisbeyan.utils.StaticData
import com.thy.servisbeyan.utils.Status
import com.thy.servisbeyan.utils.Utils.Companion.getToken
import com.thy.servisbeyan.utils.Utils.Companion.showToast

class ManuelCheckActivity : AppCompatActivity(), View.OnClickListener,
    ServiceSelectBottomSheet.ServiceItemClick,
    CampusSelectBottomSheet.CampusItemClick,
    CategorySelectBottomSheet.CategoryItemClick,
    TextWatcher {
    lateinit var activityManuelCheckBinding: ActivityManuelCheckBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityManuelCheckBinding = ActivityManuelCheckBinding.inflate(layoutInflater)
        setContentView(activityManuelCheckBinding.root)
        bindView()
        setupViewModel()
    }

    @SuppressLint("SetTextI18n")
    private fun bindView() {
        val myService = ResponseSingleton.getInstance().myService
        if (myService != null) {
            serviceId = myService.id
            serviceName = myService.servisAdi
            serviceCode = myService.servisCode
            serviceType = myService.servisTipi
            StaticData.vehicleCategoryName = myService.vehicleCategory?.name.toString()
            StaticData.serviceCampusName = myService.serviceCampus?.name.toString()
            StaticData.serviceCampusCode = myService.serviceCampus?.code!!
            StaticData.vehicleCategoryCode = myService.vehicleCategory?.code!!

            activityManuelCheckBinding.tvCampus.text = StaticData.serviceCampusName
            activityManuelCheckBinding.tvCategory.text = StaticData.vehicleCategoryName


            activityManuelCheckBinding.tvService.text =
                "$serviceCode - $serviceName"
            if ("RİNG".equals(StaticData.vehicleCategoryName, ignoreCase = true)) {
                activityManuelCheckBinding.linearLayout.visibility = View.GONE
                activityManuelCheckBinding.tvDirectionTitle.visibility = View.GONE

                activityManuelCheckBinding.llVardiya.visibility = View.GONE
                activityManuelCheckBinding.tvVardiya.visibility = View.GONE

            } else if ("VARDİYA SERVİS".equals(
                    StaticData.vehicleCategoryName,
                    ignoreCase = true
                )
            ) {
                activityManuelCheckBinding.linearLayout.visibility = View.VISIBLE
                activityManuelCheckBinding.tvDirectionTitle.visibility = View.VISIBLE

                activityManuelCheckBinding.llVardiya.visibility = View.VISIBLE
                activityManuelCheckBinding.tvVardiya.visibility = View.VISIBLE
            } else if ("NORMAL SERVİS".equals(
                    StaticData.vehicleCategoryName,
                    ignoreCase = true
                )
            ) {
                activityManuelCheckBinding.linearLayout.visibility = View.VISIBLE
                activityManuelCheckBinding.tvDirectionTitle.visibility = View.VISIBLE

                activityManuelCheckBinding.llVardiya.visibility = View.GONE
                activityManuelCheckBinding.tvVardiya.visibility = View.GONE
            }
        }

        activityManuelCheckBinding.tvManuelToolbarTitle.text =
            getString(R.string.add_service_manuel_1) + " " + getString(R.string.add_service_manuel_2)

        activityManuelCheckBinding.btnDirection1.setOnClickListener(this)
        activityManuelCheckBinding.btnDirection2.setOnClickListener(this)
        activityManuelCheckBinding.cvService.setOnClickListener(this)
        activityManuelCheckBinding.btnCheckIn.setOnClickListener(this)
        activityManuelCheckBinding.ivBackButton.setOnClickListener(this)
        activityManuelCheckBinding.edtSeat.addTextChangedListener(this)
        activityManuelCheckBinding.cardViewCampus.setOnClickListener(this)
        activityManuelCheckBinding.cardViewCategory.setOnClickListener(this)
        activityManuelCheckBinding.btnVardiya1.setOnClickListener(this)
        activityManuelCheckBinding.btnVardiya2.setOnClickListener(this)
        activityManuelCheckBinding.btnVardiya3.setOnClickListener(this)

        addNecessaryAreaStars()
    }

    private lateinit var mainViewModel: MainViewModel
    private fun setupViewModel() {
        mainViewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(ApiUtils.apiEndpoint()))
        ).get(MainViewModel::class.java)
    }

    private fun checkInCall(checkInRequest: CheckInRequest) {
        mainViewModel.checkIn(getToken(), checkInRequest)
            .observe(this@ManuelCheckActivity, {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCCESS -> {
                            activityManuelCheckBinding.progressBar.visibility = View.GONE
                            showSuccessDialog("$serviceCode - $serviceName - ($serviceSelectType) servisine checkin işlemi yapılmıştır.")
                        }
                        Status.ERROR -> {
                            activityManuelCheckBinding.progressBar.visibility = View.GONE
                            showToast(this@ManuelCheckActivity, it.message)
                        }
                        Status.LOADING -> {
                            activityManuelCheckBinding.progressBar.visibility = View.VISIBLE
                        }
                    }
                }
            })
    }

    private fun showSuccessDialog(title: String) {
        val dialog = Dialog(this@ManuelCheckActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog_check_in_success)
        val tvDescription = dialog.findViewById(R.id.tv_desc) as TextView
        val btnOk = dialog.findViewById(R.id.btn_ok) as Button
        tvDescription.text = title
        btnOk.setOnClickListener {
            dialog.dismiss()
            finish()
        }
        dialog.show()
        dialog.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    private fun showCheckDialog(title: String) {
        val dialog = Dialog(this@ManuelCheckActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.custom_dialog_check_in)
        val tvDescription = dialog.findViewById(R.id.tv_desc) as TextView
        val tvDescription2 = dialog.findViewById(R.id.tv_desc_2) as TextView
        val btnYes = dialog.findViewById(R.id.btn_yes) as Button
        val btnNo = dialog.findViewById(R.id.btn_no) as Button
        tvDescription.text = title
        tvDescription2.text = getString(R.string.dialog_desc)
        btnYes.setOnClickListener {
            dialog.dismiss()
            checkIn()
        }
        btnNo.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
        dialog.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    private fun showServiceBottomSheet() {
        val serviceSelectBottomSheet: ServiceSelectBottomSheet =
            ServiceSelectBottomSheet.newInstance()
        serviceSelectBottomSheet.show(supportFragmentManager, ServiceSelectBottomSheet.TAG)
    }

    private fun showCampusBottomSheet() {
        val campusSelectBottomSheet: CampusSelectBottomSheet =
            CampusSelectBottomSheet.newInstance()
        campusSelectBottomSheet.show(supportFragmentManager, CampusSelectBottomSheet.TAG)
    }

    private fun showCategoryBottomSheet() {
        val categorySelectBottomSheet: CategorySelectBottomSheet =
            CategorySelectBottomSheet.newInstance()
        categorySelectBottomSheet.show(supportFragmentManager, CategorySelectBottomSheet.TAG)
    }

    private var serviceId = -1
    private var serviceName = ""
    private var serviceCode = ""
    private var serviceType = ""
    private var serviceSelectType = ""
    private var directionId = 1
    private var otherDirectionId = 3
    private var isOtherSelected = false
    private var seatId = -1
    private var vardiyaId = 1
    private var vardiyaType1 = ""

    @SuppressLint("SetTextI18n")
    override fun onServiceItemClick(id: Int, name: String?, code: String?, type: String?) {
        serviceId = id
        serviceName = name!!
        serviceCode = code!!
        serviceType = type!!
        activityManuelCheckBinding.tvService.text = "$code - $name"
    }

    @SuppressLint("SetTextI18n")
    override fun onCampusItemClick(code: Int, name: String?) {
        StaticData.serviceCampusCode = code
        StaticData.serviceCampusName = name!!
        activityManuelCheckBinding.tvCampus.text = name
        activityManuelCheckBinding.tvService.text = "Seçiniz"
        serviceId = -1
        serviceName = ""
        serviceCode = ""
        serviceType = ""
    }

    override fun onCategoryItemClick(code: Int, name: String?) {
        StaticData.vehicleCategoryCode = code
        StaticData.vehicleCategoryName = name!!
        activityManuelCheckBinding.tvCategory.text = name
        activityManuelCheckBinding.tvService.text = "Seçiniz"
        if ("RİNG".equals(StaticData.vehicleCategoryName, ignoreCase = true)) {
            activityManuelCheckBinding.linearLayout.visibility = View.GONE
            activityManuelCheckBinding.tvDirectionTitle.visibility = View.GONE

            activityManuelCheckBinding.llVardiya.visibility = View.GONE
            activityManuelCheckBinding.tvVardiya.visibility = View.GONE

        } else if ("VARDİYA SERVİS".equals(
                StaticData.vehicleCategoryName,
                ignoreCase = true
            )
        ) {
            activityManuelCheckBinding.linearLayout.visibility = View.VISIBLE
            activityManuelCheckBinding.tvDirectionTitle.visibility = View.VISIBLE

            activityManuelCheckBinding.llVardiya.visibility = View.VISIBLE
            activityManuelCheckBinding.tvVardiya.visibility = View.VISIBLE
        } else if ("NORMAL SERVİS".equals(
                StaticData.vehicleCategoryName,
                ignoreCase = true
            )
        ) {
            activityManuelCheckBinding.linearLayout.visibility = View.VISIBLE
            activityManuelCheckBinding.tvDirectionTitle.visibility = View.VISIBLE

            activityManuelCheckBinding.llVardiya.visibility = View.GONE
            activityManuelCheckBinding.tvVardiya.visibility = View.GONE
        }
    }

    private fun validateCheckIn(): Boolean {
        if (serviceId != -1) {
            return true
        }
        return false
    }

    private fun checkIn() {
        val checkInRequest = CheckInRequest()
        checkInRequest.beaconId = ""
        if (isOtherSelected) {
            checkInRequest.direction = otherDirectionId
        } else {
            checkInRequest.direction = directionId
        }
        checkInRequest.isManuel = true
        checkInRequest.seatNumber = seatId
        checkInRequest.serviceId = serviceId
        checkInRequest.date = StaticData.selectDate
        checkInRequest.shiftType = vardiyaId

        checkInCall(checkInRequest)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.iv_back_button -> {
                finish()
            }
            R.id.cv_service -> {
                showServiceBottomSheet()
            }
            R.id.cardViewCampus -> {
                showCampusBottomSheet()
            }
            R.id.cardViewCategory -> {
                showCategoryBottomSheet()
            }
            R.id.btn_check_in -> {
                if (validateCheckIn()) {
                    if (StaticData.vehicleCategoryName != "RİNG" && serviceSelectType == "" || StaticData.vehicleCategoryName=="VARDİYA SERVİS" && vardiyaType1=="") {
                        showToast(
                            this@ManuelCheckActivity,
                            getString(R.string.login_validate_error)
                        )
                    } else {
                        if (seatId in 1..55) {
                            val myService = ResponseSingleton.getInstance().myService
                            if (serviceId == myService.id) {
                                checkIn()
                            } else {
                                showCheckDialog(
                                    "${myService.servisCode} - ${myService.servisAdi} - (${myService.servisTipi}) servisine kayıtlısınız, " +
                                            "beyanınızı $serviceCode - $serviceName - ($serviceSelectType) servisine yapıyorsunuz."
                                )
                            }
                        } else {
                            val dialogBuilder = AlertDialog.Builder(this)
                            dialogBuilder.setMessage("Lütfen koltuk numaranızı 1 ile 55 arasında belirtiniz.")
                                .setCancelable(true)
                                .setPositiveButton("Tamam") { _, _ ->
                                }
                            val alert = dialogBuilder.create()
                            alert.show()
                        }


                    }
                } else {
                    showToast(this@ManuelCheckActivity, getString(R.string.login_validate_error))
                }
            }
            R.id.btn_direction_1 -> {
                directionId = 1
                serviceSelectType = "Gidiş"
                activityManuelCheckBinding.btnDirection1.background = ContextCompat.getDrawable(
                    this@ManuelCheckActivity,
                    R.drawable.bg_select_direction
                )
                activityManuelCheckBinding.btnDirection2.background = ContextCompat.getDrawable(
                    this@ManuelCheckActivity,
                    R.drawable.bg_un_select_direction
                )
                activityManuelCheckBinding.btnDirection1.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.white
                    )
                )
                activityManuelCheckBinding.btnDirection2.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.blue
                    )
                )
            }
            R.id.btn_direction_2 -> {
                directionId = 2
                serviceSelectType = "Dönüş"
                activityManuelCheckBinding.btnDirection1.background = ContextCompat.getDrawable(
                    this@ManuelCheckActivity,
                    R.drawable.bg_un_select_direction
                )
                activityManuelCheckBinding.btnDirection2.background = ContextCompat.getDrawable(
                    this@ManuelCheckActivity,
                    R.drawable.bg_select_direction
                )
                activityManuelCheckBinding.btnDirection1.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.blue
                    )
                )
                activityManuelCheckBinding.btnDirection2.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.white
                    )
                )
            }

            R.id.btn_vardiya_1 -> {
                vardiyaId = 1
                vardiyaType1 = "Sabah"
                activityManuelCheckBinding.btnVardiya1.background = ContextCompat.getDrawable(
                    this@ManuelCheckActivity,
                    R.drawable.bg_select_direction
                )
                activityManuelCheckBinding.btnVardiya2.background = ContextCompat.getDrawable(
                    this@ManuelCheckActivity,
                    R.drawable.bg_un_select_direction
                )

                activityManuelCheckBinding.btnVardiya3.background = ContextCompat.getDrawable(
                    this@ManuelCheckActivity,
                    R.drawable.bg_un_select_direction
                )

                activityManuelCheckBinding.btnVardiya1.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.white
                    )
                )
                activityManuelCheckBinding.btnVardiya2.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.blue
                    )
                )

                activityManuelCheckBinding.btnVardiya3.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.blue
                    )
                )

            }
            R.id.btn_vardiya_2 -> {
                vardiyaId = 2
                vardiyaType1 = "Öğle"
                activityManuelCheckBinding.btnVardiya1.background = ContextCompat.getDrawable(
                    this@ManuelCheckActivity,
                    R.drawable.bg_un_select_direction
                )
                activityManuelCheckBinding.btnVardiya2.background = ContextCompat.getDrawable(
                    this@ManuelCheckActivity,
                    R.drawable.bg_select_direction
                )

                activityManuelCheckBinding.btnVardiya3.background = ContextCompat.getDrawable(
                    this@ManuelCheckActivity,
                    R.drawable.bg_un_select_direction
                )

                activityManuelCheckBinding.btnVardiya1.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.blue
                    )
                )
                activityManuelCheckBinding.btnVardiya2.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.white
                    )
                )

                activityManuelCheckBinding.btnVardiya3.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.blue
                    )
                )
            }
            R.id.btn_vardiya_3 -> {
                vardiyaId = 3
                vardiyaType1 = "Gece"
                activityManuelCheckBinding.btnVardiya1.background = ContextCompat.getDrawable(
                    this@ManuelCheckActivity,
                    R.drawable.bg_un_select_direction
                )
                activityManuelCheckBinding.btnVardiya2.background = ContextCompat.getDrawable(
                    this@ManuelCheckActivity,
                    R.drawable.bg_un_select_direction
                )

                activityManuelCheckBinding.btnVardiya3.background = ContextCompat.getDrawable(
                    this@ManuelCheckActivity,
                    R.drawable.bg_select_direction
                )

                activityManuelCheckBinding.btnVardiya1.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.blue
                    )
                )
                activityManuelCheckBinding.btnVardiya2.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.blue
                    )
                )

                activityManuelCheckBinding.btnVardiya3.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.white
                    )
                )
            }
        }
    }

    private fun addNecessaryAreaStars() {
        // setTextColor(activityManuelCheckBinding.tvSeatTitle, 6, 7, R.color.red)
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    private var edtValue = ""

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        edtValue = s.toString()
        if (!"".equals(edtValue, true)) {
            if (edtValue.toInt() > 55 || edtValue.toInt() == 0) {
                seatId = -1
                activityManuelCheckBinding.edtSeat.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.red
                    )
                )
            } else {
                seatId = (s.toString()).toInt()
                activityManuelCheckBinding.edtSeat.setTextColor(
                    ContextCompat.getColor(
                        this@ManuelCheckActivity,
                        R.color.black
                    )
                )
            }
        }

    }

    override fun afterTextChanged(s: Editable?) {
    }
}