package com.thy.servisbeyan.ui.view.bottomsheet;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.thy.servisbeyan.data.model.response.allservice.Data;
import com.thy.servisbeyan.data.model.response.allservice.ServiceCampus;
import com.thy.servisbeyan.databinding.BottomSheetCampusSelectBinding;
import com.thy.servisbeyan.ui.view.adapter.CampusSelectAdapter;
import com.thy.servisbeyan.utils.ResponseSingleton;
import com.thy.servisbeyan.utils.listener.CampusSelectItemListener;

import java.util.List;

public class CampusSelectBottomSheet extends BottomSheetDialogFragment implements CampusSelectItemListener {
    public static final String TAG = "CampusSelectBottomSheet";
    private CampusItemClick itemClick;
    BottomSheetCampusSelectBinding bottomSheetCampusSelectBinding;

    public static CampusSelectBottomSheet newInstance() {
        return new CampusSelectBottomSheet();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        bottomSheetCampusSelectBinding = BottomSheetCampusSelectBinding.inflate(getLayoutInflater());
        return bottomSheetCampusSelectBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAdapter();
    }


   public void setAdapter() {
        List<ServiceCampus> campusList = ResponseSingleton.getInstance().getCampusList();
        CampusSelectAdapter campusSelectAdapter = new CampusSelectAdapter(campusList, this);
        bottomSheetCampusSelectBinding.rvCampus.setLayoutManager(new LinearLayoutManager(requireContext()));
        bottomSheetCampusSelectBinding.rvCampus.setAdapter(campusSelectAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bottomSheetCampusSelectBinding = null;
    }

    @Override
    public void onItemCampusSelectListener(int code, String name) {
        itemClick.onCampusItemClick(code, name);
        dismiss();
    }

    public interface CampusItemClick {
        void onCampusItemClick(int code, String name);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CampusItemClick) {
            itemClick = (CampusItemClick) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ItemClickListener");
        }
    }
}
