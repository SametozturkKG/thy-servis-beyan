package com.thy.servisbeyan.ui.view.bottomsheet;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.thy.servisbeyan.data.model.response.allservice.VehicleCategory;
import com.thy.servisbeyan.databinding.BottomSheetCategorySelectBinding;
import com.thy.servisbeyan.ui.view.adapter.CategorySelectAdapter;
import com.thy.servisbeyan.utils.ResponseSingleton;
import com.thy.servisbeyan.utils.listener.CategorySelectItemListener;

import java.util.List;

public class CategorySelectBottomSheet extends BottomSheetDialogFragment implements CategorySelectItemListener {
    public static final String TAG = "CategorySelectBottomSheet";
    private CategoryItemClick itemClick;
    BottomSheetCategorySelectBinding bottomSheetCategorySelectBinding;

    public static CategorySelectBottomSheet newInstance() {
        return new CategorySelectBottomSheet();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        bottomSheetCategorySelectBinding = BottomSheetCategorySelectBinding.inflate(getLayoutInflater());
        return bottomSheetCategorySelectBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAdapter();
    }


   public void setAdapter() {
        List<VehicleCategory> categoryList = ResponseSingleton.getInstance().getCategoryList();
        CategorySelectAdapter categorySelectAdapter = new CategorySelectAdapter(categoryList, this);
        bottomSheetCategorySelectBinding.rvCategory.setLayoutManager(new LinearLayoutManager(requireContext()));
        bottomSheetCategorySelectBinding.rvCategory.setAdapter(categorySelectAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bottomSheetCategorySelectBinding = null;
    }

    @Override
    public void onItemCategorySelectListener(int code, String name) {
        itemClick.onCategoryItemClick(code, name);
        dismiss();
    }

    public interface CategoryItemClick {
        void onCategoryItemClick(int code, String name);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CategoryItemClick) {
            itemClick = (CategoryItemClick) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ItemClickListener");
        }
    }
}
