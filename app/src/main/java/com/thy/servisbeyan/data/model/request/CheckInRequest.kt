package com.thy.servisbeyan.data.model.request

data class CheckInRequest(
    var beaconId: String = "",
    var serviceId: Int = -1,
    var seatNumber: Int = -1,
    var direction: Int = -1,
    var date: String = "",
    var isManuel: Boolean = false,
    var shiftType: Int = -1
)
