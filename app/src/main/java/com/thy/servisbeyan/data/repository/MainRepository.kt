package com.thy.servisbeyan.data.repository

import com.thy.servisbeyan.data.api.ApiHelper
import com.thy.servisbeyan.data.model.request.CheckInRequest
import com.thy.servisbeyan.data.model.request.MyCheckInRequest

class MainRepository(private val apiHelper: ApiHelper) {
    suspend fun getServiceDetail(token: String) = apiHelper.getServiceDetail(token)
    suspend fun getAllService(token: String) = apiHelper.getAllService(token)
    suspend fun checkIn(token: String, checkInRequest: CheckInRequest) = apiHelper.checkIn(token, checkInRequest)
    suspend fun getMyCheckIn(token: String, myCheckInRequest: MyCheckInRequest) = apiHelper.getMyCheckIn(token, myCheckInRequest)
    suspend fun deleteCheckIn(token: String,checkInId: String) = apiHelper.deleteCheckIn(token, checkInId)


}