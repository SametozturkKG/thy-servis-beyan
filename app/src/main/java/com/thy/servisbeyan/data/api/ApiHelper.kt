package com.thy.servisbeyan.data.api

import com.thy.servisbeyan.data.model.request.CheckInRequest
import com.thy.servisbeyan.data.model.request.LoginRequest
import com.thy.servisbeyan.data.model.request.MyCheckInRequest

class ApiHelper(private var apiService: ApiEndpoint) {
    suspend fun login(loginRequest: LoginRequest) = apiService.login(loginRequest)
    suspend fun getServiceDetail(token: String) = apiService.getServiceDetail(token)
    suspend fun getAllService(token: String) = apiService.getAllService(token)
    suspend fun checkIn(token: String, checkInRequest: CheckInRequest) = apiService.checkIn(token, checkInRequest)
    suspend fun getMyCheckIn(token: String, myCheckInRequest: MyCheckInRequest) = apiService.getMyCheckIn(token, myCheckInRequest)
    suspend fun deleteCheckIn(token: String, checkInId: String) = apiService.deleteCheckIn(token, checkInId)
}