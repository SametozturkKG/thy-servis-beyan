package com.thy.servisbeyan.data.model.response.allservice

import com.google.gson.annotations.SerializedName

data class VehicleCategory(
    @SerializedName("code")
    var code: Int = 0,
    @SerializedName("name")
    var name: String = ""
)
