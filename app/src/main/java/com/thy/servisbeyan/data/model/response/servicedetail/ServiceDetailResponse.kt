package com.thy.servisbeyan.data.model.response.servicedetail


import com.google.gson.annotations.SerializedName

data class ServiceDetailResponse(
    @SerializedName("data")
    var `data`: Data = Data(),
    @SerializedName("result")
    var result: Boolean = false,
    @SerializedName("resultCode")
    var resultCode: Int = 0,
    @SerializedName("resultMessage")
    var resultMessage: String = ""
)