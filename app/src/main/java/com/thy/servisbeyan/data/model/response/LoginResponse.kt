package com.thy.servisbeyan.data.model.response


import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("nameSurname")
    var nameSurname: String = "",
    @SerializedName("result")
    var result: Boolean = false,
    @SerializedName("resultCode")
    var resultCode: Int = 0,
    @SerializedName("resultMessage")
    var resultMessage: String = "",
    @SerializedName("token")
    var token: String = ""
)