package com.thy.servisbeyan.data.model.response.allservice


import com.google.gson.annotations.SerializedName

data class AllServiceResponse(
    @SerializedName("data")
    var `data`: List<Data> = listOf(),
    @SerializedName("result")
    var result: Boolean = false,
    @SerializedName("resultCode")
    var resultCode: Int = 0,
    @SerializedName("resultMessage")
    var resultMessage: String = ""
)