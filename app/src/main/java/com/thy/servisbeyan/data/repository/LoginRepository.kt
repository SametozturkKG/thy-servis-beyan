package com.thy.servisbeyan.data.repository

import com.thy.servisbeyan.data.api.ApiHelper
import com.thy.servisbeyan.data.model.request.LoginRequest

class LoginRepository(private val apiHelper: ApiHelper) {
    suspend fun login(loginRequest: LoginRequest) = apiHelper.login(loginRequest)
}