package com.thy.servisbeyan.data.api

import com.thy.servisbeyan.utils.StaticData
import com.turkishairlines.loginclient.AccessManager

object ApiUtils {
    private const val BASE_URL = "https://www4.thy.com/sts-gateway/"
    private const val BASE_URL_ACM = "https://wsmob.thy.com/sts-checkin/"

    fun apiEndpoint():ApiEndpoint{
        return if(StaticData.accessManager!=null){
            StaticData.accessManager!!.retrofit.create(ApiEndpoint::class.java)
        }else{
            RetrofitClient.getClient(BASE_URL)!!.create(ApiEndpoint::class.java)
        }
    }
}