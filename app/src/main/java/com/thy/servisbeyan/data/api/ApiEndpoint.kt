package com.thy.servisbeyan.data.api

import com.thy.servisbeyan.data.model.request.CheckInRequest
import com.thy.servisbeyan.data.model.request.LoginRequest
import com.thy.servisbeyan.data.model.request.MyCheckInRequest
import com.thy.servisbeyan.data.model.response.DefaultResponse
import com.thy.servisbeyan.data.model.response.LoginResponse
import com.thy.servisbeyan.data.model.response.allservice.AllServiceResponse
import com.thy.servisbeyan.data.model.response.mycheckin.MyCheckInResponse
import com.thy.servisbeyan.data.model.response.servicedetail.ServiceDetailResponse
import retrofit2.http.*

interface ApiEndpoint {
    @POST("login")
    suspend fun login(@Body loginRequest: LoginRequest): LoginResponse

    @GET("api/getServiceDetail")
    suspend fun getServiceDetail(@Header("Mobile-Authorization") token: String): ServiceDetailResponse

    @GET("api/getAllService")
    suspend fun getAllService(@Header("Mobile-Authorization") token: String): AllServiceResponse

    @POST("api/shuttleCheckIn")
    suspend fun checkIn(
        @Header("Mobile-Authorization") token: String,
        @Body checkInRequest: CheckInRequest
    ): DefaultResponse

    @POST("api/getMyCheckIn")
    suspend fun getMyCheckIn(
        @Header("Mobile-Authorization") token: String,
        @Body myCheckInRequest: MyCheckInRequest
    ): MyCheckInResponse

    @GET("api/deleteCheckIn/{id}")
    suspend fun deleteCheckIn(
        @Header("Mobile-Authorization") token: String,
        @Path("id") checkInId: String
    ): DefaultResponse
}
