package com.thy.servisbeyan.data.model.response.mycheckin


import com.google.gson.annotations.SerializedName

data class MyCheckInResponse(
    @SerializedName("data")
    var `data`: MutableList<Data> = mutableListOf(),
    @SerializedName("result")
    var result: Boolean = false,
    @SerializedName("resultCode")
    var resultCode: Int = 0,
    @SerializedName("resultMessage")
    var resultMessage: String = ""

)