package com.thy.servisbeyan.data.model.response.allservice


import com.google.gson.annotations.SerializedName
import com.kontakt.sdk.android.common.profile.ISecureProfile

data class Data(
    @SerializedName("beaconList")
    var beaconList: List<String> = listOf(),
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("kapasite")
    var kapasite: Int = 0,
    @SerializedName("plaka")
    var plaka: String = "",
    @SerializedName("servisAdi")
    var servisAdi: String = "",
    @SerializedName("servisCode")
    var servisCode: String = "",
    @SerializedName("servisTipi")
    var servisTipi: String = "",
    var beacon: ISecureProfile? = null,
    @SerializedName("serviceCampus")
    var serviceCampus: ServiceCampus? = null,
    @SerializedName("vehicleCategory")
   var vehicleCategory: VehicleCategory? = null

)