package com.thy.servisbeyan.data.model.response


import com.google.gson.annotations.SerializedName

data class DefaultResponse(
    @SerializedName("data")
    var `data`: Any = Any(),
    @SerializedName("result")
    var result: Boolean = false,
    @SerializedName("resultCode")
    var resultCode: Int = 0,
    @SerializedName("resultMessage")
    var resultMessage: String = ""
)