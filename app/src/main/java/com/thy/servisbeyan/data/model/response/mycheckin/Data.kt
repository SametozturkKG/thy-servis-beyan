package com.thy.servisbeyan.data.model.response.mycheckin


import com.google.gson.annotations.SerializedName
import com.thy.servisbeyan.data.model.response.allservice.ServiceCampus
import com.thy.servisbeyan.data.model.response.allservice.VehicleCategory

data class Data(
    @SerializedName("date")
    var date: String = "",
    @SerializedName("direction")
    var direction: String = "",
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("serviceCode")
    var serviceCode: String = "",
    @SerializedName("serviceName")
    var serviceName: String = "",
    @SerializedName("time")
    var time: String = "",
    @SerializedName("seat")
    var seat: String = "",
    @SerializedName("servisTipi")
    var servisType: String = "",
    @SerializedName("shiftType")
    var shiftType: Int = 0,
    @SerializedName("serviceCampus")
    var serviceCampus: ServiceCampus? = null,
    @SerializedName("vehicleCategory")
    var vehicleCategory: VehicleCategory? = null
)