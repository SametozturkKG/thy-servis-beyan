package com.thy.servisbeyan.data.model.response.servicedetail


import com.google.gson.annotations.SerializedName
import com.thy.servisbeyan.data.model.response.allservice.ServiceCampus
import com.thy.servisbeyan.data.model.response.allservice.VehicleCategory

data class Data(
    @SerializedName("beaconList")
    var beaconList: Any = Any(),
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("kapasite")
    var kapasite: Int = 0,
    @SerializedName("plaka")
    var plaka: String = "",
    @SerializedName("servisAdi")
    var servisAdi: String = "",
    @SerializedName("servisCode")
    var servisCode: String = "",
    @SerializedName("servisTipi")
    var servisTipi: String = "",
    @SerializedName("serviceCampus")
    var serviceCampus: ServiceCampus? = null,
    @SerializedName("vehicleCategory")
    var vehicleCategory: VehicleCategory? = null
)