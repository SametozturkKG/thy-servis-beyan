package com.thy.servisbeyan.data.model.request

data class LoginRequest(
    var username: String = "",
    var password: String = "",
    var deviceId: String = "",
    var companyId: Int = -1
)
