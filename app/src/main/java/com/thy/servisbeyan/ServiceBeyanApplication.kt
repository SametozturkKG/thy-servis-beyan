package com.thy.servisbeyan

import android.app.Application
import android.content.Context
import com.kontakt.sdk.android.common.KontaktSDK
import com.thy.servisbeyan.utils.PrefManager
import com.turkishairlines.loginclient.AccessManager
import com.turkishairlines.loginclient.AccessManagerFactory
import com.turkishairlines.loginclient.config.AccessNetworkConfig

class ServiceBeyanApplication : Application() {

    val SERVICE_DOMAIN = "MobileServiceDomain"
    val AGENT_APP_ID = "OICSSOApp1"
    val READ_TIMEOUT = 120
    val CONNECT_TIMEOUT = 120

    override fun onCreate() {
        super.onCreate()
        PrefManager.with(this)
        KontaktSDK.initialize(this)
    }

    private var accessManager: AccessManager? = null

    @Synchronized
    fun getAccessManager(context: Context): AccessManager? {
        if (accessManager == null) {
            val networkConfig: AccessNetworkConfig = AccessNetworkConfig(
                BuildConfig.LOGIN_BASE_URL,
                BuildConfig.BASE_URL,
                SERVICE_DOMAIN,
                AGENT_APP_ID,
                BuildConfig.APP_ID,
                BuildConfig.BASE_STORE_URL
            )
                .setLogEnabled(BuildConfig.LOG_ENABLED)
                .setConnectTimeout(CONNECT_TIMEOUT)
                .setReadTimeout(READ_TIMEOUT)
                .setMockEnabled(false)

            accessManager = AccessManagerFactory.getAccessManager(context, networkConfig)
        }
        return accessManager
    }
}
